package com.java;

public class Main {

    public static void main(String[] args) {
	    try {
	        // The code where you are expecting some exceptions
        } catch(Exception e) {
	        // for exception handling code is required and will be written here
        } finally {
	        // will execute always
        }
    }
}
